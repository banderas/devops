# sa.it-academy.by
Main purpose of this repository is collaboration of homework  for study groups
## Links
- How to join to the course [IT Academy site](https://www.it-academy.by/)
- Shared slides: [GDRIVE](https://drive.google.com/open?id=0B7-pec-Rldg3fmZyRTdHb1NzUmwzcUxGdVNBNEpndTFVa00wcHFVLUlIbHpiS0FrbEd5QzQ)

## Contacts & communication 
- Slack channels for groups [sa-itacademy-by.slack.com](https://sa-itacademy-by.slack.com), invite only
- Author's email [pluhin@gmail.com](pluhin@gmail.com)

## CI/CD
